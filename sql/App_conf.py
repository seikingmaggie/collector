import json

# 读取配置
def read_config():
    with open("sql\sqlconfig.json") as json_file:
        config = json.load(json_file)
    return config

# 从参数更新配置
def update_config(config):
    with open("sql\sqlconfig.json", 'w') as json_file:
        json.dump(config, json_file, indent=4,ensure_ascii=False)
    return None

# 从全局变量更新配置
def save_config():
    update_config(config)

# 把配置从配置文件读取到变量
config = read_config()

# globals().update(config) #把app_conf中的config字典中的键值对变成变量

if __name__ == '__main__':
    try:
        read_config()
        print(config['host'])
    except KeyboardInterrupt:
        print("over")
