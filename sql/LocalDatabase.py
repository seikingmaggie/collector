#python3
#!/usr/bin/env python

import datetime
import json
import time
from typing import Any
import pymysql
from decorator.singleton import SingletonType
from sql.App_conf import *
from sql.ComplexEncoder import *


# 连接本地MySQL数据库，进行数据存储。
#        树莓派本地表存储传感器数据，视频，图片不存MySQL，或者使用序列化本地存储传感器数据
# 
class LocalDatabase(metaclass = SingletonType):
    local_db = None
    local_cursor = None


    def get_local_cursor(self):
        return self.local_cursor

    def __init__(self) -> None:

        try:
            self.local_db = pymysql.connect(host='localhost',
                                            user=config['local_user'],
                                            password=config['local_password'],
                                            database=config['local_database']
                                            )
            

            self.local_cursor = self.local_db.cursor()

            
            # SQL_SELECT_DEVICE = "select * from data "
            # self.local_cursor.execute(SQL_SELECT_DEVICE)
            # result = self.local_cursor.fetchone()
            #  print(result)
            
        except KeyboardInterrupt:
            pass


    def insertData(self,air_temperature='NULL',humidity='NULL',temperature='NULL',heart_rate='NULL',blood_oxygen='NULL')->None:
        sqlstr = "insert into data(air_temperature,humidity,temperature,heart_rate,blood_oxygen) values("+air_temperature+","+humidity+","+temperature+","+heart_rate+","+blood_oxygen+")"
        self.local_cursor.execute(sqlstr)
        self.local_db.commit()

    def insertData(self,data):
        i=0
        for dtt in data:
                # 空值处理
            if(dtt==None):
                data[i] = 'NULL'
            i=i+1
                
        sqlstr = "insert into data(air_temperature,humidity,temperature,heart_rate,blood_oxygen) values("+str(data[2])+","+str(data[3])+","+str(data[4])+","+str(data[5])+","+str(data[6])+")"
        self.local_cursor.execute(sqlstr)
        self.local_db.commit()

    def insertDataList(self, dataList):
        for dt in dataList:
            i=0
            for dtt in dt:
                # 空值处理
                if(dtt==None):
                    dt[i] = 'NULL'
                i=i+1
                
            sqlstr = "insert into data(air_temperature,humidity,temperature,heart_rate,blood_oxygen) values("+str(dt[2])+","+str(dt[3])+","+str(dt[4])+","+str(dt[5])+","+str(dt[6])+")"
            self.local_cursor.execute(sqlstr)

        self.local_db.commit()

    def get_not_uplaod(self):
        # 返回list
        sqlstr = "select * from data where upload=0"
        self.local_cursor.execute(sqlstr)
        result = self.local_cursor.fetchall()
        # result_data = json.dumps(result,cls=ComplexEncoder,indent=4,separators=(',', ': '))
        # print(result_data)    #返回json
        return list(result)
    
    def set_uplaod(self,data_id):
        sqlstr = "update data set upload=1 where id = " +str(data_id)
        self.local_cursor.execute(sqlstr)
        
    
    def close(self):
        self.local_db.close()
    


if __name__ == '__main__':
    try:
        # 直接运行有模块路径问题 ，去掉 sql.  运行
        L = LocalDatabase()
        
        data = json.loads(L.get_not_uplaod())

        print(type(data))
        print(data[0][1])

    except KeyboardInterrupt:
        print("over")
