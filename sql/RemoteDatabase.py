#python3


#!/usr/bin/env python

import time
import pymysql
from sql.LocalDatabase import LocalDatabase
from decorator.singleton import SingletonType
from sql.App_conf import *

# 从本地数据库获取数据，上传至远程数据库

class RemoteDatabase(metaclass = SingletonType):
    local_db = None
    remote_db = None
    remote_cursor = None
    # 连接到服务器后
    def __init__(self) -> None:
        globals().update(config) #把app_conf中的config字典中的键值对变成变量

        try:
            self.remote_db = pymysql.connect(host=config['host'],
                                            user=config['user'],
                                            password=config['password'],
                                            database=config['database']
                                            )
            self.remote_cursor = self.remote_db.cursor()

            with open("config.json") as json_file:
                device_cfg = json.load(json_file)
            self.device_id = device_cfg["device_id"]

            
            SQL_SELECT_DEVICE = "select * from Device where id=" +str(self.device_id)
            self.remote_cursor.execute(SQL_SELECT_DEVICE)
            result = self.remote_cursor.fetchall()
            print(result)
            # print((result[0][-1]))

        except Exception:
            pass

    def insertData(self,datalist):
        pass

    def uploadData(self):
        
        self.local_db = LocalDatabase()
        notupload = self.local_db.get_not_uplaod()
        # print(type(notupload[1]))

        for dt in notupload:
            i=0
            dt = list(dt)
            for dtt in dt:
                # 空值处理
                if(dtt==None):
                    dt[i] = 'NULL'
                i=i+1
                
            sqlstr = "insert into datarecord(device_id,data_id,create_datetime,air_temperature,humidity,temperature,heart_rate,blood_oxygen) values("+str(self.device_id)+","+str(dt[0])+", \'"+str(dt[1])+"\',"+str(dt[2])+","+str(dt[3])+","+str(dt[4])+","+str(dt[5])+","+str(dt[6])+")"
            self.remote_cursor.execute(sqlstr)
            self.local_db.set_uplaod(dt[0])

        self.local_db.local_db.commit()
        self.remote_db.commit()

        

if __name__ == '__main__':
    try:
        R = RemoteDatabase()
    except KeyboardInterrupt:
        print("over")



