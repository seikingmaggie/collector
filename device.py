#python3
# -*- coding: utf-8 -*- 

#开机自动运行

from sql.LocalDatabase import LocalDatabase
from sql.RemoteDatabase import RemoteDatabase
from sensor.Camera import Cmara
from sensor.Heart_rate_sensor import Heart_rate_sensor
from sensor.Temp_humi_sensor import Temp_humi_sensor
from threadsensor.Database_Thread import DataBase_Thread
from threadsensor.Heart_rate_sensor_Thread import Heart_rate_sensor_Thread
from threadsensor.Temp_humi_sensor_Thread import Temp_humi_sensor_Thread

device_id = 1

def checkEenviroment():
    pass
    

if __name__ == '__main__':
    #检查本地数据库，检查各个传感器是否工作，是否能使用智能API，是否能连接服务器远程数据库，
    
    sensor = Heart_rate_sensor()
    sensor = Temp_humi_sensor()
    capclass = Cmara()

    #定时检测温度湿度，相机实时检测人体姿态，异常警报，
    thread_h_r_sensor = Heart_rate_sensor_Thread(1, "心率检测", 1)
    thread_t_h_sensor = Temp_humi_sensor_Thread(1,'温度湿度',2)
    thread_database = DataBase_Thread(1,'数据上传',5)
    
    
    #语音识别，用户主动检测血氧，心率，体温数据       当血氧模块检测到数据时自动记录

    pass