#!/usr/bin/env python
#python3.9
import base64
import time
from turtle import right
from urllib.parse import urlencode
import cv2
import numpy as np
#from sklearn.feature_extraction import img_to_graph
from aip import AipFace,AipBodyAnalysis

from aip.ocr import AipOcr
from aip.speech import AipSpeech

APP_ID = "25922986"
API_KEY = "eDj3uMtgeQqhomRO4hWBN2Uu"
SK = ""#百度apiSK
face_client = AipFace(APP_ID,API_KEY,SK)
body_client = AipBodyAnalysis(APP_ID,API_KEY,SK)
text_client = AipOcr(APP_ID,API_KEY,SK)
speech_client = AipSpeech(APP_ID,API_KEY,SK)

path = "https://aip.baidubce.com/rest/2.0/face/v3/detect";
#acksecctoke 每半个月重新获取一次
astok = "24.f52af2a06e0b59aab7c5185a19c591b9.2592000.1652528376.282335-25922986"

def get_file_content(filePath):
    # b64encode是编码，b64decode是解码 
    with open(filePath,'r') as f:
        #base64_data = base64.b64encode(f.read)    
        return f.read() #base64_data
def image_to_base64(image_np):
    image = cv2.imencode('.jpg',image_np)[1]
    image_code = str(base64.b64encode(image))[2:-1]
    return image_code
def base64_to_image(base64_code):
    # base64解码
    img_data = base64.b64decode(base64_code)
    # 转换为np数组
    img_array = np.fromstring(img_data, np.uint8)
    # 转换成opencv可用格式
    img = cv2.imdecode(img_array, cv2.COLOR_RGB2BGR)
    return img
def cvimg_to_base64(imgpath):
    image_np = cv2.imread(imgpath)
    cvimg = cv2.cvtColor(image_np, cv2.COLOR_BGR2RGB)
    cvimg = cv2.imencode('.jpg',cvimg)[1]
    # = cv2.imencode('.png',img)
    cvimg = str(base64.b64encode(cvimg))[2:-1]
    return cvimg
def my_urlencode(str) :
    reprStr = repr(str).replace(r'\x', '%')
    return reprStr[1:-1]

#imageType = "BASE64"
imageType = "URL"
imgpath = "./theimages/img666.jpg"
groupId = "group1"
userId = "user1"
#人脸注册
#face_result = face_client.addUser(image,imageType,groupId,userId);

#调用人脸检测
def the_detect_BASE64(imagepath):
    imageType = "BASE64"
    options = {}
    options["face_field"] = "age,beauty,expression,face_shape"#,gender,glasses,landmark,landmark72,landmark150,quality,eye_status,emotion,face_type"
    info = "年龄、美貌、表情" #、脸型、性别、眼镜、landmark、landmark72、landmark150、质量、眼睛状况、情绪、脸型"
    options["max_face_num"] = 2
    #options["face_type"] = "LIVE"
    #options["liveness_control"] = "LOW"
    face_result = face_client.detect(cvimg_to_base64(imagepath),imageType,options)    
    #print(face_result)
    return face_result

imagepath =   "https://thumbnail0.baidupcs.com/thumbnail/cae419cebu739bf6978762189676f30f?fid=1785360920-250528-579170747959006&time=1649937600&rt=sh&sign=FDTAER-DCb740ccc5511e5e8fedcff06b081203-HXZlLCyIL%2FxE3FgoteDydj5AxLY%3D&expires=8h&chkv=0&chkbd=0&chkpc=&dp-logid=102400104148739816&dp-callid=0&file_type=0&size=c710_u400&quality=100&vuk=-&ft=video"

def the_detect_URL(imagepath):
    imageType = "URL"
    options = {}
    options["face_field"] = "age,beauty"
    options["max_face_num"] = 2
    options["face_type"] = "LIVE"
    options["liveness_control"] = "LOW"
    face_result = face_client.detect(imagepath,imageType,options)
    print(face_result)
    return face_result

def get_file_content(filePath):
    with open(filePath, 'rb') as fp:
        return fp.read()

#人体分析
def body_analysis(imagepath):
    img = get_file_content(imagepath)
    body_result = body_client.bodyAnalysis(img)
    print(body_result)


#人脸检测每秒只能调用2次
#调用人脸检测，并输出图片
def print_theimage_info(imagepath):
    img = cv2.imread(imagepath)
    face_result = the_detect_BASE64(imagepath)
    if 'result' in face_result:
        if face_result['error_msg'] == "SUCCESS":
            print(face_result)
            left = (face_result['result']['face_list'][0]['location']['left'])
            top =  (face_result['result']['face_list'][0]['location']['top'])
            width =(face_result['result']['face_list'][0]['location']['width'])
            height=(face_result['result']['face_list'][0]['location']['height'])
            left = int(left)
            top = int(top)
            width = int(width)
            height = int(height)
            cv2.rectangle(img,(left,top),(left+width,top+height),(255,0,0),2)
            img_str = "age " + str(face_result['result']['face_list'][0]['age']) + "\nbeauty " + str(face_result['result']['face_list'][0]['beauty'])
            cv2.putText(img,img_str,(left,top),cv2.FONT_HERSHEY_COMPLEX,1,(10,80,100),1)
            #cv2.imshow("img",img)
            #cv2.waitKey(0)
    else:
        print(type(face_result))

    return img

def theimage_info(img):
    img_base = image_to_base64(img)
    imageType = "BASE64"
    options = {}
    options["face_field"] = "age,beauty,expression,face_shape"#,gender,glasses,landmark,landmark72,landmark150,quality,eye_status,emotion,face_type"
    info = "年龄、美貌、表情" #、脸型、性别、眼镜、landmark、landmark72、landmark150、质量、眼睛状况、情绪、脸型"
    options["max_face_num"] = 2
    #options["face_type"] = "LIVE"
    #options["liveness_control"] = "LOW"
    face_result = face_client.detect(img_base,imageType,options)#人脸检测
    if 'result' in face_result:
        if face_result['error_msg'] == "SUCCESS":
            print(face_result)
            left = (face_result['result']['face_list'][0]['location']['left'])
            top =  (face_result['result']['face_list'][0]['location']['top'])
            width =(face_result['result']['face_list'][0]['location']['width'])
            height=(face_result['result']['face_list'][0]['location']['height'])
            left = int(left)
            top = int(top)
            width = int(width)
            height = int(height)
            cv2.rectangle(img,(left,top),(left+width,top+height),(255,0,0),2)
            img_str = "age " + str(face_result['result']['face_list'][0]['age']) + "\nbeauty " + str(face_result['result']['face_list'][0]['beauty'])
            cv2.putText(img,img_str,(left,top),cv2.FONT_HERSHEY_COMPLEX,1,(10,255,100),1)
            cv2.imshow("img",img)
    else:
        print(type(face_result))


    return img

def cap_look():
    cap = cv2.VideoCapture(0)
    while cap.isOpened():
        ret,frame = cap.read()
        theimage_info(frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        time.sleep(0.5)
    cap.release()
    cv2.waitKey(0)

def image_look(imgpath):
    image = get_file_content(imgpath)
    #res_image = text_client.general(image)
    #print(res_image)
    res_image_g = text_client.basicGeneral(image)#文字提取
    print(res_image_g)

def speech_listen(filepath):
    thefile = get_file_content(filepath)
    res_listen = speech_client.asr(thefile,'wav',1600)
    print(res_listen)
    
def print_boay_point(img,point_list):
    for point in point_list:
        print(point_list[point])
        if point_list[point]["score"]>0.7:
            #print((type(point_list[point]["y"]),point_list[point]["x"]))
            cv2.circle(img,(int(point_list[point]["x"]),int(point_list[point]["y"])),2,(250,0,0),2)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

if __name__=="__main__":
    cap_look()

    cv2.waitKey(0)
