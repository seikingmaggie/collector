import cv2
import time

class tcap:
    def my_urlencode(self,str) :
        reprStr = repr(str).replace(r'\x', '%')
        return reprStr[1:-1]
    def test(self):
        
        ss = "1234567"
        st = self.my_urlencode(ss)
        print(type(st))

        cap = cv2.VideoCapture(0)
        while cap.isOpened():
            ret,frame = cap.read()
            
            cv2.imshow("video",frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        cap.release()
        cv2.waitKey(0)

def main():
    capclass = tcap()
    capclass.test()
            
def destroy():
    print("000")

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print("over")
        destroy()     