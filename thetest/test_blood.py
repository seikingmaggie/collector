import serial
'''
AT+T\r\n	得到当前温度
AT+HEART\r\n	得到当前心率
AT+SPO2\r\n	得到当前血氧
'''
def main():
    ser = serial.Serial('/dev/ttyUSB0',9600,timeout = 0.5)
    print(ser)
    print(ser.isOpen())
    while(True):
  
        ser.write("AT+HEART\r\n".encode())
        ser.inWaiting()
        data=ser.readlines()
        print(data)
        
        ser.write("AT+T\r\n".encode())
        ser.inWaiting()
        data=ser.readlines()
        print(data)

        
        ser.write("AT+SPO2\r\n".encode())
        ser.inWaiting()
        data=ser.readlines()
        print(data)

        
if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print("over")
        #destroy()
