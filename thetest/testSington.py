import threading

from decorator.singleton import SingletonType




class task(metaclass = SingletonType):
    def __init__(self,name):
        self.name = name


for i in range(10):
    t = threading.Thread(target=task,args=[i,])
    t.start()