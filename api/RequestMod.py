import RPi.GPIO as GPIO
import time
import json, requests
import datetime

channel =2 #GPIO2
data = []
j = 0

GPIO.setmode(GPIO.BCM)

time.sleep(1)

GPIO.setup(channel, GPIO.OUT)
GPIO.output(channel, GPIO.LOW)
time.sleep(0.02)
GPIO.output(channel, GPIO.HIGH)
GPIO.setup(channel, GPIO.IN)

while GPIO.input(channel) == GPIO.LOW:
    continue
while GPIO.input(channel) == GPIO.HIGH:
    continue

while j < 40:
    k = 0
    while GPIO.input(channel) == GPIO.LOW:
        continue
    while GPIO.input(channel) == GPIO.HIGH:
        k+=1
    if k > 100:
        break
    if k < 8:
        data.append(0)
    else:
        data.append(1)
    j += 1

print ("sensor is working.")
#print data

humidity_bit = data[0:8]
humidity_point_bit = data[8:16]
temperature_bit = data[16:24]
temperature_point_bit = data[24:32]
check_bit = data[32:40]

humidity = 0
humidity_point = 0
temperature = 0
temperature_point = 0
check = 0

for i in range(8):
    humidity += humidity_bit[i] * 2 ** (7-i)
    humidity_point += humidity_point_bit[i] * 2 ** (7-i)
    temperature += temperature_bit[i] * 2 ** (7-i)
    temperature_point += temperature_point_bit[i] * 2 ** (7-i)
    check += check_bit[i] * 2 ** (7-i)

tmp = humidity + humidity_point + temperature + temperature_point

if check == tmp:
    print ("temperature :", temperature, "*C, humidity :", humidity, "%")
    res='{value:%f}'% temperature

    with open('/home/pi/Desktop/data.txt', 'a') as outfile:
        json.dump(res, outfile)
    outest=open('/home/pi/Desktop/data.txt','a')
    outest.write(res)
    outest.close
    #print(res)
else:
    print ("wrong")
  #print "temperature :", temperature, "*C, humidity :", humidity, "% check :", check, ", tmp :", tmp

# Post Data to webpage
       
rqs_headers={'Content-Type': 'application/json'}
requrl ='http://192.168.1.3:8000/temperature_api/'
new_data = {
    "captime": datetime.datetime.now(),
    "captemperature": temperature
}

class ComplexEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return obj.strftime('%Y-%m-%d %H:%M:%S')
        elif isinstance(obj, datetime.date):
            return obj.strftime('%Y-%m-%d')
        else:
            return json.JSONEncoder.default(self, obj)
        
test_data = json.dumps(new_data, cls=ComplexEncoder)

response = requests.post(url=requrl, headers=rqs_headers, data=test_data)

GPIO.cleanup()