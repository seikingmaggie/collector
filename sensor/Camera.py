#python3
# -*- coding: utf-8 -*-  
import cv2

# 需要挂载 mount /dev/mmcblk0p1 /boot

class Cmara:
    
    def my_urlencode(self,str) :
        reprStr = repr(str).replace(r'\x', '%')
        return reprStr[1:-1]
    
    def gesture_recognition(self):
        # 姿态识别
        pass

    def get_face(self):
        # 人脸识别，     辨别老客户，新面孔，不给人脸识别，不保存心率信息 
        pass
    
    def test(self):
        #
        cap = cv2.VideoCapture(0)
        while cap.isOpened():
            ret,frame = cap.read()
            
            cv2.imshow("video",frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        cap.release()
        cv2.waitKey(0)
            
    def destroy():
        print("000")

if __name__ == '__main__':
    try:
        capclass = Cmara()
        capclass.test()
    except KeyboardInterrupt:
        print("over")
        capclass.destroy()   