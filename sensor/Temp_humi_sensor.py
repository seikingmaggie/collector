#python3
# -*- coding: gbk -*- 

import Adafruit_DHT
from decorator.singleton import SingletonType

#温度，湿度传感器 
class Temp_humi_sensor(metaclass = SingletonType):
    pin = 4
    temp = 0  #10倍温度
    humi = 0  #10倍湿度
    
    sensor = 11

    def __init__(self) -> None:
        self.temp,self.humi = Adafruit_DHT.read_retry(self.sensor,self.pin)

    def __init__(self,pin=4) -> None:
        self.pin = pin
        self.temp,self.humi = Adafruit_DHT.read_retry(self.sensor,self.pin)

    def get_temp_humi(self):
        self.humi,self.temp = Adafruit_DHT.read_retry(self.sensor,self.pin)
        return self.humi*0.1,self.temp*0.1
    
    def get_data(self):
        return self.humi*0.1,self.temp*0.1 
    
    def test(self):
        print(self.get_temp())

if __name__ == '__main__':
    try:
        sensor = Temp_humi_sensor()
        sensor.test()
    except KeyboardInterrupt:
        print("over")