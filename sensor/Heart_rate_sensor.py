#python3
# -*- coding: utf-8 -*- 
import serial

from decorator.singleton import SingletonType

class Heart_rate_sensor(metaclass = SingletonType):
    usbid = 0  #usb端口号
    usb = '/dev/ttyUSB' + str(usbid)
    temp = 0
    rate = 0
    spo2 = 0
    
    def __init__(self) -> None:
        self.ser = serial.Serial(self.usb,9600,timeout = 0.5) #初始化串口
        # 对象实例化后，一直检测，当检测到有数据时，自动保存。
        self.temp = self.get_temp()
        self.rate = self.get_heart_rate()
        self.spo2 = self.get_spo2()
        


    def get_temp(self):
        self.ser.write("AT+T\r\n".encode())
        self.ser.in_waiting()
        temp = self.ser.readlines()
        return temp
    
    
    def get_heart_rate(self):
        self.ser.write("AT+HEART\r\n".encode())
        self.ser.in_waiting()
        rate = self.ser.readlines()
        return rate
    
    def get_spo2(self):
        self.ser.write("AT+SPO2\r\n".encode())
        self.ser.in_waiting()
        spo2 = self.ser.readlines()
        return spo2
    
    def test(self):
        print(self.ser)
        print(self.ser.isOpen())
        while(True):
            print(self.get_heart_rate())
            print(self.get_spo2())
            print(self.get_temp())



if __name__ == '__main__':
    try:
        sensor = Heart_rate_sensor()
        sensor.test()
    except KeyboardInterrupt:
        print("over")
    

    

