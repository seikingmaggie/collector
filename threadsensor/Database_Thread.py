#python3

import threading
import time
from LocalDatabase import LocalDatabase
from RemoteDatabase import RemoteDatabase

from sensor.Heart_rate_sensor import Heart_rate_sensor
from sensor_data.SensorData import SensorData
from sensor.Temp_humi_sensor import Temp_humi_sensor
import sensor.VoiceBroadcastingModule


class DataBase_Thread(threading.Thread):
    #数据上传


    def __init__(self, threadID, name, delay=1):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.delay = delay*60  #延迟 分钟
        self.local_db = LocalDatabase() 
        self.remote_db = RemoteDatabase()

    def uploadData(self):
        self.remote_db.uploadData()

    
    def run(self):
    
        print ("开启Heart_rate_sensor_Thread线程： " + self.name)

        while True:
            self.uploadData()
            time.sleep(self.delay)

            

            
        