#python3

import threading
import time
from LocalDatabase import LocalDatabase

from sensor.Heart_rate_sensor import Heart_rate_sensor
from sensor_data.SensorData import SensorData
from sensor.Temp_humi_sensor import Temp_humi_sensor
import sensor.VoiceBroadcastingModule


class Temp_humi_sensor_Thread(threading.Thread):
    #当检测到有数据时，进行记录

    hrsensor  = None
    thsensor = None
    local_db = None
    data = None


    def __init__(self, threadID, name, delay=1):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.delay = delay*60  #延迟 分钟
        self.thsensor = Temp_humi_sensor()
        self.local_db = LocalDatabase() 
        self.data = SensorData()


    def updateData(self):
        (self.data['air_temperature'],self.data['humidity'])  = self.thsensor.get_temp_humi()


    def run(self):
    
        print ("开启Heart_rate_sensor_Thread线程： " + self.name)

        

        while True:
            self.updateData()
            self.local_db.insertDataList(self.data)
            time.sleep(self.delay)

            

            
        