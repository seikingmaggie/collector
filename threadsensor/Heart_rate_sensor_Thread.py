#python3

import threading
import time
from LocalDatabase import LocalDatabase

from sensor.Heart_rate_sensor import Heart_rate_sensor
from sensor_data.SensorData import SensorData
from sensor.Temp_humi_sensor import Temp_humi_sensor
import sensor.VoiceBroadcastingModule


class Heart_rate_sensor_Thread(threading.Thread):
    # 持续检测rate和spo2数据
    #当检测到有数据时，进行记录

    hrsensor  = None
    thsensor = None
    local_db = None
    data = None


    def __init__(self, threadID, name, delay=1):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.delay = delay  #延迟 秒
        self.hrsensor = Heart_rate_sensor()
        self.thsensor = Temp_humi_sensor()
        self.local_db = LocalDatabase() 
        self.data = SensorData()



        

    def updateData(self):
        self.data['temperature']  = self.hrsensor.get_temp()
        self.data['heart_rate']  = self.hrsensor.get_heart_rate()
        self.data['blood_oxygen']  = self.hrsensor.get_spo2()
        (self.data['air_temperature'],self.data['humidity'])  = self.thsensor.get_temp_humi()


    def run(self):
    
        print ("开启Heart_rate_sensor_Thread线程： " + self.name)
        
        sensor.VoiceBroadcastingModule.SetReader()
        sensor.VoiceBroadcastingModule.SetVolume(10)
        

        while True:
            self.updateData()
            if self.data['heart_rate']!=None and self.data['heart_rate']>1:
                self.local_db.insertDataList(self.data)

                sensor.VoiceBroadcastingModule.Speech_text('心率'+str(self.data['heart_rate']),sensor.VoiceBroadcastingModule.EncodingFormat_Type["GB2312"])
                while sensor.VoiceBroadcastingModule.GetChipStatus() != sensor.VoiceBroadcastingModule.ChipStatus_Type['ChipStatus_Idle']:#等待当前语句播报结束
                    time.sleep(0.1) 
            time.sleep(self.delay)

            

            
        