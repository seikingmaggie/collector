

import json


class SensorData:
    #字典类型
    data = {
    "id" : 1,
    "create_time": "NULL",
    "air_temperature" : 0,
    "humidity" : 0,
    "temperature" : 0,
    "heart_rate" : 0,
    "blood_oxygen" : 0,
    "upload" : 0
    }

    def __init__(self) -> None:
        self.data.id = 1;
        with open('config.json','r') as f :
            self.data.id = json.load(f)['device_id']

    def setData(self,dataid,create_time,air_temperature,humidity,temperature,heart_rate,blood_oxygen,upload):
        self.data['id'] = dataid;
        self.data['create_time'] = create_time;
        self.data['air_temperature'] = air_temperature;
        self.data['humidity'] = humidity;
        self.data['temperature'] = temperature;
        self.data['heart_rate'] = heart_rate;
        self.data['blood_oxygen'] = blood_oxygen;
        self.data['upload'] = upload;
    
    def setDataList(self,dataList:list):
        #dataList 为list
        #从本地数据库取出的数据，要转为SensorData,再post到服务器
        self.data['id'] = dataList[0];
        self.data['create_time'] = dataList[1];
        self.data['air_temperature'] = dataList[2];
        self.data['humidity'] =  dataList[3];
        self.data['temperature'] =  dataList[4];
        self.data['heart_rate'] =  dataList[5];
        self.data['blood_oxygen'] =  dataList[6];
        self.data['upload'] =  dataList[7];
        return dataList
        

    def getjson(self):
        json_str = json.dumps(self.data,indent=4,ensure_ascii=False)
        # print(json_str)
        return json_str
    
    def getdict(self):
        # print(self.data)
        return self.data

    def getlist(self):
        # print(self.data.values())
        return list(self.data.values())



